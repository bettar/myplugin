//  myPlugin.mm
//  myPlugin
//
//  Created by Alessandro on 10 Dec 2020
//

#import "myPlugin.h"

// Don't modify this line
#define PLUGIN_ID "FC45939A-9654-4F71-A915-1B4BF0EBCA79"

@implementation myPlugin

- (void) initPlugin
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (long) filterImage:(NSString*) menuName
{
    NSLog(@"%s, menuName: %@", __PRETTY_FUNCTION__, menuName);
    return EXIT_SUCCESS;
}

- (NSString *)description
{
    return @"My plugin description";
}

@end
